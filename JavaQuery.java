import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class JavaQuery {
    public static void main(String[] args){
     List<Delivery1> delivery = readDeliveriesFileData();
     List<Matches> match = readMatchesFileData();
        System.out.println("                                   query 1                         ");
     totalmatchesplayedperyear(match);
        System.out.println("                                   query 2                         ");
        totalmatchwonbyeachteam(match);
        System.out.println("                                    query 3                         ");
      extraRunsConcededPerTeam(delivery);
        System.out.println("                                    query 4                         ");
      topeconomicalplayer(delivery);

    }
    public static List<Delivery1> readDeliveriesFileData(){
        List<Delivery1> deliveries = new ArrayList<>();
        try
        {
            BufferedReader csvReader = new BufferedReader(new FileReader(".idea/Dataset/deliveries.csv"));
            List<String> bowler = new ArrayList<>();
            String row = null;
            int i = 0;
            while ((row = csvReader.readLine()) != null) {
                if (i > 0) {
                    String[] arr = row.split(",");
                    Delivery1 d = new Delivery1();

                    d.setMatch_id(arr[0]);
                    d.setInning(arr[1]);
                    d.setBatting_team(arr[2]);
                    d.setBowling_team(arr[3]);
                    d.setOver(arr[4]);
                    d.setBall(arr[5]);
                    d.setBatsman(arr[6]);
                    d.setNon_striker(arr[7]);
                    d.setBowler(arr[8]);
                    d.setIs_super_over(arr[9]);
                    d.setWide_runs(arr[10]);
                    d.setBye_runs(arr[11]);
                    d.setLegbye_runs(arr[12]);
                    d.setNoball_runs(arr[13]);
                    d.setPenalty_runs(arr[14]);
                    d.setBatsman_runs(arr[15]);
                    d.setExtra_runs(arr[16]);
                    d.setTotal_runs(arr[17]);
                    deliveries.add(d);
                }
                i++;
            }
            csvReader.close();
        } catch(FileNotFoundException e)
        {
            e.printStackTrace();
        } catch(IOException e)
        {
            e.printStackTrace();
        }
    return deliveries;
    }
    public static List<Matches> readMatchesFileData(){
        List<Matches> match = new ArrayList<>();
        try
        {
            BufferedReader csvReader = new BufferedReader(new FileReader(".idea/Dataset/matches.csv"));

            String row = null;
            int i = 0;
            while ((row = csvReader.readLine()) != null) {
                if (i > 0) {
                    String[] arr = row.split(",");
                    Matches m =new Matches();
                   m.setId(arr[0]);
                   m.setSeason(arr[1]);
                   m.setCity(arr[2]);
                   m.setDate(arr[3]);
                   m.setTeam1(arr[4]);
                   m.setTeam2(arr[5]);
                   m.setToss_winner(arr[6]);
                   m.setToss_decision(arr[7]);
                   m.setResult(arr[8]);
                   m.setDl_applied(arr[9]);
                   m.setWinner(arr[10]);
                    match.add(m);
                }
                i++;
            }
            csvReader.close();
        } catch(FileNotFoundException e)
        {
            e.printStackTrace();
        } catch(IOException e)
        {
            e.printStackTrace();
        }
        return match;
    }
    public static void totalmatchesplayedperyear(List<Matches> match){
        LinkedList<Integer> year = new LinkedList<>();
        LinkedList<Integer> noofmatchesplayed = new LinkedList<>();
        int counter = 0;
        Integer season ;
        for(int i=0 ; i< match.size() ; i++){
            Matches m = new Matches();
              m = match.get(i);
              season = Integer.parseInt(m.getSeason());
            if(!year.contains(season)){
                year.add(season);
                if(i > 0) {
                    noofmatchesplayed.add(counter);
                }
                counter=0;
            }
            counter++;
        }

        for(int i = 0 ; i< year.size() && i<noofmatchesplayed.size() ; i++ ){
            System.out.println(year.get(i) + " " + noofmatchesplayed.get(i));
        }
    }
    public static void totalmatchwonbyeachteam(List<Matches> match){
        Map <String, Integer> totalmatchwonperteam = new HashMap<>();
        Integer matcheswon ;
        String team ;
        for(int i=0 ; i < match.size() ; i++){
            Matches m = new Matches();
            m = match.get(i);
            team = m.getToss_winner();
            //System.out.println(team);
            if(!totalmatchwonperteam.containsKey(team)){
                totalmatchwonperteam.put(team,0);

            }
            matcheswon = totalmatchwonperteam.get(team);
            totalmatchwonperteam.replace(team,matcheswon+1);
        }

        for (Map.Entry<String,Integer> entry : totalmatchwonperteam.entrySet()) {
            System.out.println(entry.getKey() + " ,  " + entry.getValue());
        }
    }
    public static void extraRunsConcededPerTeam(List<Delivery1> delivery){
        Map<String, Integer> extrarun = new HashMap();
       Integer matchId = 0 ;
       Integer extraruns = 0;
       String teamName = null;
       Delivery1 d = new Delivery1();
        for (int i = 0; i < delivery.size(); i++) {
            d = delivery.get(i);
            matchId = Integer.parseInt(d.getMatch_id());

            if(matchId >= 576 && matchId <= 636){
                teamName = d.getBatting_team();
                extraruns = Integer.parseInt(d.getExtra_runs());
                if(!extrarun.containsKey(teamName)){
                    extrarun.put(d.getBatting_team(),extraruns);
                } else{
                    extraruns = extraruns + extrarun.get(teamName);
                    extrarun.put(teamName, extraruns);
                    extraruns = 0;
                }
            }
        }

        for (Map.Entry<String,Integer> entry : extrarun.entrySet()) {
            System.out.println(entry.getKey() + " ,  " + entry.getValue());
        }
    }
    public static void topeconomicalplayer(List<Delivery1> deliveries){

        Integer counter = 0;
        Integer matchId = 0;
        Integer totalrun = 0 ;
        Map<String , Integer> ball = new HashMap<>();
        Map<String , Integer> runs = new HashMap<>();
        Map<String , Double> economyrate = new HashMap<>();
        for(int i = 0 ; i < deliveries.size() ; i++) {
            Delivery1 d = new Delivery1();
            d = deliveries.get(i);
            matchId = Integer.parseInt(d.getMatch_id());
            if (matchId >= 518 && matchId <= 576) {
                totalrun = Integer.parseInt(d.getTotal_runs());
                counter++;
                if (!ball.containsKey(d.getBowler())) {
                    ball.put(d.getBowler(), counter);
                    runs.put(d.getBowler(), totalrun);
                    economyrate.put(d.getBowler(), 0.0);
                    counter = 0;
                } else {
                    counter = counter + ball.get(d.getBowler());
                    totalrun = runs.get(d.getBowler()) + totalrun - Integer.parseInt(d.getLegbye_runs()) - Integer.parseInt(d.getBye_runs());
                    ball.replace(d.getBowler(), counter);
                    runs.replace(d.getBowler(), totalrun);
                    counter = 0;
                }
            }
        }
        Double t = 0.0;
        Double  b = 0.0;
        Double rate = 0.0;
        for (Map.Entry<String,Double> entry : economyrate.entrySet()) {

            t =Double.valueOf( runs.get(entry.getKey()));
            b = Double.valueOf(ball.get(entry.getKey()));
            b = b / 6;
            rate = Double.valueOf(t / b);
            economyrate.replace(entry.getKey(),rate);
            System.out.println(entry.getKey() + " ,  " + entry.getValue());
        }
    }
}
